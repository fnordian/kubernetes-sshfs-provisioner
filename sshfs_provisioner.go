package main

import (
	"os"

	"github.com/kubernetes-incubator/external-storage/lib/controller"

	"flag"
	"time"

	"fmt"
	"os/exec"

	"path"
	"syscall"

	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/pkg/api/v1"
	"k8s.io/client-go/rest"
)

type sshfsProvisioner struct {
	identity   string
	mountPath  string
	sshKey     int
	sshHostKey int
}

func createMountDirOnHost(dir string) error {
	return exec.Command("nsenter", "--mount=/media/host/proc/1/ns/mnt", "--",
		"mkdir", "-p", dir).Run()
}

func removeMountDirOnHost(dir string) error {
	return exec.Command("nsenter", "--mount=/media/host/proc/1/ns/mnt", "--",
		"rmdir", dir).Run()
}

func buildMountCommand(username string, host string, remotePath string, mountPath string, identityFile string, hostKeyFile string) *exec.Cmd {
	return exec.Command("/symlinkwrapper",
		"-t", identityFile, "-l", "/tmp/key",
		"-t", hostKeyFile, "-l", "/tmp/hostkey",
		"--",
		"sshfs",
		fmt.Sprintf("%s@%s:%s", username, host, remotePath),
		mountPath,
		"-o", fmt.Sprintf("IdentityFile=/tmp/key"),
		"-o", "auto_unmount",
		"-o", "nomap=ignore",
		"-o", fmt.Sprintf("UserKnownHostsFile=/tmp/hostkey"))
}

func (p *sshfsProvisioner) mountSshfs(username string, host string, remotePath string, mountPath string) error {

	cmd := buildMountCommand(username, host, remotePath, mountPath,
		"ssh/id_rsa.storage", "ssh/hostkey.storage")

	output, err := cmd.CombinedOutput()

	if len(output) != 0 {
		glog.Infof("mount output: %s", string(output))
	}

	return err
}

func (p *sshfsProvisioner) Provision(options controller.VolumeOptions) (*v1.PersistentVolume, error) {
	//mount -t fuse.sshfs -o IdentityFile=$HOME/.ssh/id_rsa,nomap=ignore,allow_other user@host:/tmp mountpoint
	path := path.Join(p.mountPath, options.PVName)

	glog.Infof("creating %s on host", path)

	if err := createMountDirOnHost(path); err != nil {
		glog.Infof("error creating directory: %s", err)
		return nil, err
	}

	glog.Infof("provisioning sshfs with %s@%s %s %s",
		options.Parameters["username"],
		options.Parameters["host"],
		options.Parameters["basepath"],
		path)

	if err := p.mountSshfs(options.Parameters["username"], options.Parameters["host"],
		options.Parameters["basepath"], path); err != nil {

		glog.Errorf("unable to mount: %s", err)
		return nil, err
	}

	pv := &v1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name: options.PVName,
			Annotations: map[string]string{
				"sshfsProvisionerIdentity": p.identity,
			},
		},
		Spec: v1.PersistentVolumeSpec{
			PersistentVolumeReclaimPolicy: options.PersistentVolumeReclaimPolicy,
			AccessModes:                   options.PVC.Spec.AccessModes,
			Capacity: v1.ResourceList{
				v1.ResourceName(v1.ResourceStorage): options.PVC.Spec.Resources.Requests[v1.ResourceName(v1.ResourceStorage)],
			},
			PersistentVolumeSource: v1.PersistentVolumeSource{
				HostPath: &v1.HostPathVolumeSource{
					Path: path,
				},
			},
		},
	}

	return pv, nil
}

func buildUMountCommand(mountPath string) *exec.Cmd {
	return exec.Command("/symlinkwrapper", "--", "umount", mountPath)
}

func (p *sshfsProvisioner) umountSshfs(mountPath string) error {

	cmd := buildUMountCommand(mountPath)

	output, err := cmd.CombinedOutput()

	if len(output) != 0 {
		glog.Infof("umount output: %s", string(output))
	}

	return err
}

func (p *sshfsProvisioner) Delete(volume *v1.PersistentVolume) error {
	p.umountSshfs(volume.Spec.PersistentVolumeSource.HostPath.Path)
	removeMountDirOnHost(volume.Spec.PersistentVolumeSource.HostPath.Path)
	return nil
}

func NewSshFsProvisioner() controller.Provisioner {
	nodeName := os.Getenv("NODE_NAME")
	if nodeName == "" {
		glog.Fatal("env variable NODE_NAME must be set so that this provisioner can identify itself")
	}

	nullFd, _ := syscall.Open("/dev/null", syscall.O_RDONLY, 0)
	hostKeyFd, _ := syscall.Open("/ssh/hostkey.storage", syscall.O_RDONLY, 0)
	sshKeyFd, _ := syscall.Open("/ssh/id_rsa.storage", syscall.O_RDONLY, 0)
	syscall.Close(nullFd)

	p := &sshfsProvisioner{
		mountPath:  "/tmp/sshfs-provisioner",
		identity:   nodeName,
		sshHostKey: hostKeyFd,
		sshKey:     sshKeyFd,
	}

	return p
}

const (
	resyncPeriod              = 15 * time.Second
	provisionerName           = "fnordian/sshfs_provisioner"
	exponentialBackOffOnError = false
	failedRetryThreshold      = 5
	leasePeriod               = controller.DefaultLeaseDuration
	retryPeriod               = controller.DefaultRetryPeriod
	renewDeadline             = controller.DefaultRenewDeadline
	termLimit                 = controller.DefaultTermLimit
)

func main() {
	flag.Parse()
	flag.Set("logtostderr", "true")

	// Create an InClusterConfig and use it to create a client for the controller
	// to use to communicate with Kubernetes
	config, err := rest.InClusterConfig()
	if err != nil {
		glog.Fatalf("Failed to create config: %v", err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatalf("Failed to create client: %v", err)
	}

	// The controller needs to know what the server version is because out-of-tree
	// provisioners aren't officially supported until 1.5
	serverVersion, err := clientset.Discovery().ServerVersion()
	if err != nil {
		glog.Fatalf("Error getting server version: %v", err)
	}

	// Create the provisioner: it implements the Provisioner interface expected by
	// the controller
	sshFsProvisioner := NewSshFsProvisioner()

	// Start the provision controller which will dynamically provision hostPath
	// PVs
	pc := controller.NewProvisionController(clientset, resyncPeriod, provisionerName, sshFsProvisioner, serverVersion.GitVersion, exponentialBackOffOnError, failedRetryThreshold, leasePeriod, renewDeadline, retryPeriod, termLimit)
	pc.Run(wait.NeverStop)
}
