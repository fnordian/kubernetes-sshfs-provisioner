

SRC:=$(shell find . -name "*.go")
BINARY:=sshfs-provisioner

all: ${BINARY} symlinkwrapper/symlinkwrapper

${BINARY}: ${SRC}
	glide install -v
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o $@
	chmod 755 $@

symlinkwrapper/symlinkwrapper: symlinkwrapper/symlinkwrapper.c
	gcc -o $@ -static symlinkwrapper/symlinkwrapper.c


clean:
	rm -rf vendor
	rm ${BINARY}

.PHONY: clean
