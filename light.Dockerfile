FROM alpine:3.5

RUN apk add --no-cache sshfs

COPY sshfs-provisioner sshfs-provisioner
COPY slw symlinkwrapper

CMD ["/sshfs-provisioner"]
