FROM lwolf/golang-glide

RUN apk add --update make
RUN apk add gcc
RUN mkdir -p /go/src/sshfs-provisioner
COPY glide.lock /go/src/sshfs-provisioner/
COPY glide.yaml /go/src/sshfs-provisioner/
RUN cd /go/src/sshfs-provisioner && glide install -v

RUN apk add libc-dev

COPY . /go/src/sshfs-provisioner
RUN cd /go/src/sshfs-provisioner && make && cp sshfs-provisioner symlinkwrapper/symlinkwrapper /

CMD ["/sshfs-provisioner"]
