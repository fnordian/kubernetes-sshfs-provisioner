#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h>


int findFileLink(char *basepath, char *targetfilename) {

	DIR* proc = opendir(basepath);
	struct dirent* ent;

	char path[30];
	char dstfile[30];
	int found = 0;

	while(proc != NULL && (ent = readdir(proc))) {
		snprintf(path, sizeof(path), "%s/%s", basepath, ent->d_name);

		int len = readlink(path, dstfile, sizeof(dstfile) - 1);
		dstfile[len] = '\0';
		if (!strcmp(dstfile, targetfilename)) {
			found = 1;
			break;
		}
	}
	
	closedir(proc);

	return found;
}

int findMyPid() {
	DIR* proc = opendir("/proc");
	struct dirent* ent;
	long tgid;
	int tmpfd;
	pid_t mypid = -1;

	char path[30];
	char tmpfile[30]; snprintf(tmpfile, sizeof(tmpfile), "/tmp/mntXXXXXXX");
	mkstemp(tmpfile);
	tmpfd = open(tmpfile, O_RDONLY, 0);

	if(proc == NULL) {
		perror("opendir(/proc)");
		return 1;
	}

	while(ent = readdir(proc)) {
		if(!isdigit(*ent->d_name))
			continue;

		snprintf(path, sizeof(path), "/proc/%s/fd", ent->d_name);

		if (findFileLink(path, tmpfile)) {
			mypid = atoi(ent->d_name);
			printf("my pid is %d\n", mypid);
			break;
		}
		
	}

	closedir(proc);

	return mypid;

}


struct link {
	char *target;
	int target_fd;
	char *linkpath;
};

int readLinksFromOpt(int argc, char *argv[], struct link *links[], int maxlinks) {
	int numberOfLinks = 0;
	int c;

	while ((c = getopt (argc, argv, "t:l:")) != -1) {
		printf("c: %c\n", c);
		switch (c)
		{
			case 't':
				printf("t\n");
				numberOfLinks++;
				if (numberOfLinks > maxlinks) {
					return numberOfLinks;
				}
				(*links)[numberOfLinks-1].target = optarg;
				
				break;
			case 'l':
				printf("l\n");
				(*links)[numberOfLinks-1].linkpath = optarg;
				break;
			default:
				abort ();
		}
	}

	return numberOfLinks;

}

void initLinks(struct link links[], int numberOfLinks) {
	for (int i = 0; i < numberOfLinks; i++) {
		links[i].target_fd = open(links[i].target, O_RDONLY, 0);
		if (links[i].target_fd < 0) {
			perror(links[i].target);
		}
	}
}

int createLink(int target_fd, const char *linkpath, pid_t mypid) {
	char openedTarget[30];

	snprintf(openedTarget, sizeof(openedTarget), "/proc/%d/fd/%d", mypid, target_fd);
	printf("link: %s <- %s\n", openedTarget, linkpath);
	if (symlink(openedTarget, linkpath)) {
		perror("symlink");
		exit(1);
	}
}
void createLinks(struct link links[], int numberOfLinks, pid_t mypid) {
	for (int i = 0; i < numberOfLinks; i++) {
		createLink(links[i].target_fd, links[i].linkpath, mypid);
	}
}

void unlinkLinks(struct link links[], int numberOfLinks) {
	for (int i = 0; i < numberOfLinks; i++) {
		unlink(links[i].linkpath);
	}
}

int main(int argc, char *argv[], char *envp[]) {

	if (argc < 4) {
		fprintf(stderr, "usage: %s -t [target] -l [linkpath] -- [cmd] [parms]...\n", argv[0]);
		exit(1);
	}

#define maxlinks 10
	struct link *links = alloca(maxlinks * sizeof(struct link));
	int numberOfLinks = 0;

	char *target = argv[1];
	char *linkpath = argv[2];
	char *cmd;
	char **params = &argv[4];
	int mntns_fd, pidns_fd;
	pid_t mypid;

	numberOfLinks = readLinksFromOpt(argc, argv, &links, maxlinks);


	cmd = argv[optind];
	params = &argv[optind];

	mntns_fd = open("/media/host/proc/1/ns/mnt", O_RDONLY| O_DIRECTORY, 0644);

	initLinks(links, numberOfLinks);

	if(setns(mntns_fd, 0)) {
		perror("setns mnt");
		exit(1);
	}

	mypid = findMyPid();

	createLinks(links, numberOfLinks, mypid);


	int pid = fork();

	switch (pid) {
		case -1:
			perror("fork");
			exit(1);
		case 0:
			if (execvpe(cmd, params, envp)) {
				perror(cmd);
				exit(1);
			}
		default:
			waitpid(pid, NULL, 0);
			unlinkLinks(links, numberOfLinks);
	}

	exit(0);
}	
